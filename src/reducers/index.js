import { combineReducers } from 'redux';
import * as ACTION_TYPES from '../constants/ActionTypes';

export const items = (state = [], action) => {
  switch (action.type) {
    case ACTION_TYPES.ADD_ITEM:
      return [...state, action.item];
    case ACTION_TYPES.DELETE_ITEM:
      return state.filter(item => item.id !== action.item.id);
    case ACTION_TYPES.EDIT_ITEM:
      return state.map(item => {
        if (item.id === action.item.id) {
          return { ...item, ...action.item };
        }
        return item;
      });
    default:
      return state;
  }
};
export default combineReducers({
  items
});
