import { items as itemReducer } from '../index.js';
import * as ACTION_TYPES from '../../constants/ActionTypes';
import faker from 'faker';
import uuidv4 from 'uuid/v4';

const initialState = [];

describe('Items reducer', () => {
  it('should return the initial state', () => {
    expect(itemReducer(undefined, {})).toEqual([]);
  });

  let item;
  beforeEach(() => {
    item = {
      name: faker.commerce.productName(),
      price: faker.commerce.price(),
      qty: faker.finance.amount(1),
      id: uuidv4()
    };
  });

  it('should handle add new item', () => {
    expect(
      itemReducer(initialState, {
        type: ACTION_TYPES.ADD_ITEM,
        item: item
      })
    ).toEqual([item]);
  });

  it('should handle edit an item', () => {
    const newItem = {
      ...item,
      name: 'Something new',
      qty: faker.commerce.price()
    };
    expect(
      itemReducer([item], {
        type: ACTION_TYPES.EDIT_ITEM,
        item: newItem
      })
    ).toEqual([newItem]);
  });

  it('should handle delete an item', () => {
    expect(
      itemReducer([item], {
        type: ACTION_TYPES.DELETE_ITEM,
        item: item
      })
    ).toEqual([]);
  });

  it('should handle unknown action', () => {
    expect(
      itemReducer([item], {
        type: 'UNKNOWN_ACTION',
        item: item
      })
    ).toEqual([item]);
  });
});
