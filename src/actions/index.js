import * as ACTION_TYPES from '../constants/ActionTypes';
import uuidv4 from 'uuid/v4';

export const updateItem = item => {
  return {
    type: ACTION_TYPES.EDIT_ITEM,
    item
  };
};

export const addItem = item => {
  return {
    type: ACTION_TYPES.ADD_ITEM,
    item: {
      ...item,
      id: uuidv4()
    }
  };
};

export const deleteItem = item => {
  return {
    type: ACTION_TYPES.DELETE_ITEM,
    item
  };
};
