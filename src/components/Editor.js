import React from 'react';
import Item from '../containers/Item';
import NewItem from '../containers/NewItem';
import PropTypes from 'prop-types';

function Editor({ items }) {
  return (
    <table>
      <thead>
        <tr>
          <th>Items</th>
          <th>Qty</th>
          <th>Price</th>
          <th>Total</th>
        </tr>
      </thead>
      <tbody>
        {items.map(item => {
          return <Item item={item} key={item.id} />;
        })}
      </tbody>
      <tfoot>
        <NewItem />
      </tfoot>
    </table>
  );
}
Editor.propTypes = {
  items: PropTypes.array.isRequired
};
export default Editor;
