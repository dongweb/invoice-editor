import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const Wrapper = styled.div`
  display: flex;
  justify-content: flex-end;
`;

const TotalWrapper = styled.ul`
  padding: 10px;
  border-top: 2px dashed black;
  border-bottom: 2px dashed black;
  list-style-type: none;
`;

const Span = styled.span`
  float: right;
`;
const calculateSubtotal = items => {
  return items.reduce((prev, item) => {
    return prev + item.price * item.qty;
  }, 0);
};

function TotalPrice({ items }) {
  const subtotal = calculateSubtotal(items);
  const tax = subtotal * 0.05;
  const total = subtotal + tax;
  return (
    <Wrapper>
      <TotalWrapper>
        <li>
          Subtotal: <Span>${parseFloat(subtotal).toFixed(2)}</Span>
        </li>
        <li>
          Tax (5%): <Span>${parseFloat(tax).toFixed(2)}</Span>
        </li>
        <li>
          Total: <Span>${parseFloat(total).toFixed(2)}</Span>
        </li>
      </TotalWrapper>
    </Wrapper>
  );
}

TotalPrice.propTypes = {
  items: PropTypes.array.isRequired
};
export default TotalPrice;
