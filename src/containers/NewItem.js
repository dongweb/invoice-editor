import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addItem } from '../actions';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const DollarSpan = styled.span`
  font-size: 12px;
`;

class NewItem extends Component {
  handleChange = e => {
    if (this.refs.qty.value && this.refs.price.value) {
      this.refs.total.value = parseFloat(
        this.refs.qty.value * this.refs.price.value
      ).toFixed(2);
    }
  };

  handleSubmit = e => {
    if (this.refs.qty.value && this.refs.price.value) {
      const newItem = {
        name: this.refs.item.value,
        qty: this.refs.qty.value,
        price: this.refs.price.value
      };
      this.props.addItem(newItem);
      this.resetInput();
    }
  };

  resetInput = () => {
    this.refs.price.value = '';
    this.refs.qty.value = '';
    this.refs.item.value = '';
    this.refs.total.value = '';
  };

  render() {
    return (
      <tr>
        <td>
          <input
            type="text"
            name="name"
            placeholder="New Item"
            ref="item"
            onChange={this.handleChange}
          />
        </td>
        <td>
          <input
            type="number"
            name="qty"
            min="0"
            onChange={this.handleChange}
            ref="qty"
          />
        </td>
        <td>
          <input
            type="number"
            name="price"
            onChange={this.handleChange}
            ref="price"
          />
        </td>
        <td>
          <DollarSpan>$</DollarSpan>
          <input ref="total" readOnly />
        </td>
        <td>
          <button onClick={this.handleSubmit}>add</button>
        </td>
      </tr>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addItem: item => dispatch(addItem(item))
  };
};

NewItem.propTypes = {
  addItem: PropTypes.func.isRequired
};

export default connect(null, mapDispatchToProps)(NewItem);
