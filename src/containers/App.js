import React, { Component } from 'react';
import Editor from '../components/Editor';
import TotalPrice from '../components/TotalPrice';
import { connect } from 'react-redux';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const AppDiv = styled.div`
  display: flex;
  width: 640px;
  justify-content: center;
  flex-direction: column;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  background-color: #fff;
  padding: 30px;
  box-shadow: inset 0 0 10px #000000;
`;

class App extends Component {
  render() {
    return (
      <AppDiv>
        <Editor items={this.props.items} />
        <TotalPrice items={this.props.items} />
      </AppDiv>
    );
  }
}
const mapStateToProps = state => {
  return {
    items: state.items
  };
};

App.propTypes = {
  items: PropTypes.array.isRequired
};

export default connect(mapStateToProps)(App);
