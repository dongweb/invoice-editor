import React, { Component } from 'react';
import { connect } from 'react-redux';
import { updateItem, deleteItem } from '../actions';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const DollarSpan = styled.span`
  font-size: 12px;
`;

class Item extends Component {
  handleChange = e => {
    const id = e.target.dataset.id;
    const value = e.target.value;
    const name = e.target.name;
    this.props.updateItem({ id, [name]: value });
  };

  deleteItem = e => {
    const id = e.target.dataset.id;
    this.props.deleteItem({ id });
  };

  calculateTotalPrice = (price, qty) => {
    const total = price * qty;
    return parseFloat(total).toFixed(2);
  };

  render() {
    return (
      <tr>
        <td>
          <input
            type="text"
            name="name"
            value={this.props.item.name}
            data-id={this.props.item.id}
            onChange={this.handleChange}
          />
        </td>
        <td>
          <input
            type="number"
            name="qty"
            min="0"
            value={this.props.item.qty}
            onChange={this.handleChange}
            data-id={this.props.item.id}
          />
        </td>
        <td>
          <DollarSpan>$</DollarSpan>
          <input
            type="number"
            name="price"
            value={this.props.item.price}
            onChange={this.handleChange}
            data-id={this.props.item.id}
          />
        </td>
        <td>
          <DollarSpan>$</DollarSpan>
          <input
            value={this.calculateTotalPrice(
              this.props.item.price,
              this.props.item.qty
            )}
            readOnly
          />
        </td>
        <td>
          <button onClick={this.deleteItem} data-id={this.props.item.id}>
            X
          </button>
        </td>
      </tr>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    updateItem: item => dispatch(updateItem(item)),
    deleteItem: item => dispatch(deleteItem(item))
  };
};

Item.propTypes = {
  item: PropTypes.shape({
    name: PropTypes.string,
    id: PropTypes.string,
    qty: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    price: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
  }),
  updateItem: PropTypes.func.isRequired,
  deleteItem: PropTypes.func.isRequired
};

export default connect(null, mapDispatchToProps)(Item);
