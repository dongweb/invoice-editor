This **Invoice Editor** single page application was built with `react` and `redux`.

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Installation
Make sure **node** and **npm** are installed. To start the app, install dependencies first,  run
```
npm install
```
then start the dev server.
```
npm start
```

You will be able to see the app at [localhost:3000](localhost:3000).

## Features
User can
- add new item 
- edit exsiting item
- delete item by clicking `X` button

Pricing will be updated on-the-fly.
## Live demo
The online demo is also available at [anthony-invoice-editor.surge.sh](http://anthony-invoice-editor.surge.sh/).

## Test
Unit tests for reducer are added under `src/reducers/__test__` folder. The test frame is `Jest` from `create-react-app`. To run the tests, run
```
npm test
```



